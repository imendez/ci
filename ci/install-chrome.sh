#!/bin/bash

# Installs chromium in debian based distros, this script
# should be run before starting the karma tests.
# It also copies the wrapper script xvfb-chromium
# and creates symbolic links to make Karma run the tests
# using the wrapper script.

# Exit if any command fails
set -xe

# Update packages
apt-get update -yqqq

# Install Chromium browser and Virtual Display emulator
apt-get install -y xvfb chromium

# Copy the wrapper script
cp ci/xvfb-chromium /usr/bin/xvfb-chromium
chmod +x /usr/bin/xvfb-chromium

# Create symbolic links so karma can run the tests using the wrapper
ln -s /usr/bin/xvfb-chromium /usr/bin/google-chrome
ln -s /usr/bin/xvfb-chromium /usr/bin/chromium-browser
